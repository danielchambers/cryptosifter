const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const dotenv = require("dotenv");

dotenv.config({ path: "./.env" });

app.use(cors());

app.use(bodyParser.json());

app.get("/api/", (req, res) => {
  res.send("Hello, World!");
});

const poolRouter = require("./routes/pool");
const epochRouter = require("./routes/epoch");
const feedRouter = require("./routes/feed");
const searchRouter = require("./routes/search");
const portfolioRouter = require("./routes/portfolio");

app.use("/api/pool", poolRouter);
app.use("/api/epoch", epochRouter);
app.use("/api/feed", feedRouter);
app.use("/api/search", searchRouter);
app.use("/api/portfolio", portfolioRouter);

app.listen(5000, () => {
  console.log("Server listening on port 5000");
});
