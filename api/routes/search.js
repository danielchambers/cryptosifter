const express = require("express");
const router = express.Router();
const { pool } = require("../db");

router.get("/discord", (req, res) => {
  const searchText = req.query.q;

  const sqlQuery = `
    SELECT
        platform, 
        platform_date, 
        discord_id AS platform_id, 
        message_project,
        message_id,
        message_channel_id,
        message_server_id,
        message_date,
        message_text
    FROM feed_item
    LEFT JOIN discord ON feed_item.discord_id = discord.message_id
    WHERE message_text ILIKE $1 OR message_project ILIKE $1
    ORDER BY platform_date DESC
  `;

  pool
    .query(sqlQuery, [`%${searchText}%`])
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).send("Error querying database");
    });
});

router.get("/youtube", (req, res) => {
  const searchText = req.query.q;

  const sqlQuery = `
    SELECT
        platform, 
        platform_date, 
        youtube_id AS platform_id, 
        video_user,
        video_channel_title,
        video_user_image,
        video_title,
        video_description,
        video_id
    FROM feed_item
    LEFT JOIN youtube ON feed_item.youtube_id = youtube.video_id
    WHERE video_user ILIKE $1 OR video_channel_title ILIKE $1 OR video_title ILIKE $1 OR video_description ILIKE $1
    ORDER BY platform_date DESC
  `;

  pool
    .query(sqlQuery, [`%${searchText}%`])
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).send("Error querying database");
    });
});

router.get("/twitter", (req, res) => {
  const searchText = req.query.q;

  const sqlQuery = `
    SELECT
        platform, 
        platform_date, 
        twitter_id AS platform_id, 
        space_user,
        space_user_name,
        space_user_id,
        space_user_image,
        space_title,
        space_id,
        space_date
    FROM feed_item
    LEFT JOIN twitter_space ON feed_item.twitter_id = twitter_space.space_id
    WHERE space_user ILIKE $1 OR space_user_name ILIKE $1 OR space_title ILIKE $1
    ORDER BY platform_date DESC
  `;

  pool
    .query(sqlQuery, [`%${searchText}%`])
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).send("Error querying database");
    });
});

router.get("/all", (req, res) => {
  const searchText = req.query.q;

  const sqlQuery = `
    SELECT
        platform, 
        platform_date, 
        COALESCE(twitter_id, youtube_id, discord_id) AS platform_id, 
        message_project,
        message_id,
        message_channel_id,
        message_server_id,
        message_date,
        message_text,
        video_user,
        video_channel_title,
        video_user_image,
        video_title,
        video_description,
        video_id,
        space_user,
        space_user_name,
        space_user_id,
        space_user_image,
        space_title,
        space_id,
        space_date
    FROM feed_item
    LEFT JOIN twitter_space ON feed_item.twitter_id = twitter_space.space_id
    LEFT JOIN discord ON feed_item.discord_id = discord.message_id
    LEFT JOIN youtube ON feed_item.youtube_id = youtube.video_id
    WHERE message_project ILIKE $1
    OR message_text ILIKE $1
    OR video_user ILIKE $1
    OR video_channel_title ILIKE $1
    OR video_title ILIKE $1
    OR video_description ILIKE $1
    OR space_user ILIKE $1
    OR space_user_name ILIKE $1
    OR space_title ILIKE $1
    ORDER BY platform_date DESC
  `;

  pool
    .query(sqlQuery, [`%${searchText}%`])
    .then((result) => {
      res.json(result.rows);
    })
    .catch((error) => {
      console.error(error);
      res.status(500).send("Error querying database");
    });
});

module.exports = router;
