const express = require("express");
const router = express.Router();
const { pool } = require("../db");
const {
  getAddressInfo,
  getAddressRewards,
  getAddressAssets,
  getAddressDelegation,
  getAsset,
} = require("../utils/blockfrost");

const { getNftFloor, getNftData } = require("../utils/jpgstore");

router.get("/account/:accountId", async (req, res) => {
  const accountId = req.params.accountId;

  try {
    const accountInfo = await getAddressInfo(accountId);
    res.json(accountInfo);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

router.get("/account/rewards/:accountId", async (req, res) => {
  const accountId = req.params.accountId;

  try {
    const accountInfo = await getAddressRewards(accountId);
    res.json(accountInfo);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

router.get("/account/token/:walletAddress", async (req, res) => {
  const walletAddress = req.params.walletAddress;

  try {
    const assets = await getAddressAssets(walletAddress);
    const tokenData = await Promise.all(
      assets.map(async (asset) => {
        const assetData = await getAsset(asset.unit);
        if (
          assetData.metadata &&
          assetData.metadata.hasOwnProperty("decimals")
        ) {
          //   console.log(assetData);
          const { policy_id, asset_name, quantity } = assetData;
          const { ticker, name, decimals, url } = assetData.metadata;
          const sqlQuery = `SELECT price FROM price WHERE policy_id ILIKE $1`;
          const token_price = await pool.query(sqlQuery, [`%${policy_id}%`]);
          const price = token_price.rows.map((row) => Object.values(row)[0]);
          return {
            policy_id,
            price: price[0],
            asset_name,
            ticker,
            name,
            decimals,
            url,
            supply: quantity,
            quantity: asset.quantity,
          };
        } else {
          return undefined;
        }
      })
    ).then((results) => results.filter((result) => result !== undefined));
    res.json(tokenData);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

router.get("/account/nft/:walletAddress", async (req, res) => {
  const walletAddress = req.params.walletAddress;

  try {
    const assets = await getAddressAssets(walletAddress);
    const tokenData = await Promise.all(
      assets.map(async (asset) => {
        const assetData = await getAsset(asset.unit);
        if (
          //   assetData.onchain_metadata_standard !== null && assetData.metadata === null
          assetData.metadata === null
        ) {
          //   console.log(assetData);
          const { asset, policy_id, fingerprint, onchain_metadata } = assetData;
          const keysToFilter = [
            "files",
            "image",
            "mediaType",
            "name",
            "src",
            "type",
            "description",
          ];
          const filteredObj = Object.keys(onchain_metadata)
            .filter((key) => !keysToFilter.includes(key))
            .reduce((result, current) => {
              result[current] = onchain_metadata[current];
              return result;
            }, {});
          const floor = await getNftFloor(policy_id);
          let floorPrice = 0;
          if (floor) {
            floorPrice = floor.floor / 1000000;
          } else {
            floorPrice = 0;
          }
          return {
            asset,
            policy_id,
            fingerprint,
            description: onchain_metadata.description,
            image: onchain_metadata.image,
            name: onchain_metadata.name,
            attributes: filteredObj,
            floor: floorPrice,
          };
        } else {
          return undefined;
        }
      })
    ).then((results) => results.filter((result) => result !== undefined));

    res.json(tokenData);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

router.get("/account/delegation/:accountId", async (req, res) => {
  const accountId = req.params.accountId;

  try {
    const accountInfo = await getAddressDelegation(accountId);
    res.json(accountInfo);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

router.get("/asset/:asset", async (req, res) => {
  const asset = req.params.asset;

  try {
    const assetData = await getAsset(asset);
    res.json(assetData);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

module.exports = router;
