const express = require("express");
const {
  getCurrentStakedPool,
  getPoolMetadata,
} = require("../utils/blockfrost");
const router = express.Router();

router.get("/staked/:poolId", async (req, res) => {
  try {
    const poolId = req.params.poolId;
    const [accountInfo, metaData] = await Promise.all([
      getCurrentStakedPool(poolId),
      getPoolMetadata(poolId),
    ]);
    accountInfo["metadata"] = metaData;
    res.json(accountInfo);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

module.exports = router;
