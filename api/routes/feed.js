const express = require("express");
const router = express.Router();
const { pool } = require("../db");

router.get("/discord", async (req, res) => {
  try {
    const { page = 1, pageSize = 10 } = req.query;
    const offset = (page - 1) * pageSize;
    const newTableQuery = `
    SELECT
        platform, 
        platform_date, 
        discord_id AS platform_id, 
        message_project,
        message_id,
        message_channel_id,
        message_server_id,
        message_date,
        message_text
    FROM feed_item
    LEFT JOIN discord ON feed_item.discord_id = discord.message_id
    WHERE platform = 'discord'
    ORDER BY platform_date DESC
    LIMIT $1
    OFFSET $2;
    `;
    const totalItems = `SELECT COUNT(*) AS "feedCount" FROM feed_item;`;
    const newTableResult = await pool.query(newTableQuery, [pageSize, offset]);
    const { rows } = await pool.query(totalItems);
    res.json({ items: newTableResult.rows, count: rows[0].feedCount });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

router.get("/youtube", async (req, res) => {
  try {
    const { page = 1, pageSize = 10 } = req.query;
    const offset = (page - 1) * pageSize;
    const newTableQuery = `
    SELECT
        platform, 
        platform_date, 
        youtube_id AS platform_id, 
        video_user,
        video_channel_title,
        video_user_image,
        video_title,
        video_description,
        video_id
    FROM feed_item
    LEFT JOIN youtube ON feed_item.youtube_id = youtube.video_id
    WHERE platform = 'youtube'
    ORDER BY platform_date DESC
    LIMIT $1
    OFFSET $2;
    `;
    const totalItems = `SELECT COUNT(*) AS "feedCount" FROM feed_item;`;
    const newTableResult = await pool.query(newTableQuery, [pageSize, offset]);
    const { rows } = await pool.query(totalItems);
    res.json({ items: newTableResult.rows, count: rows[0].feedCount });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

router.get("/twitter", async (req, res) => {
  try {
    const { page = 1, pageSize = 10 } = req.query;
    const offset = (page - 1) * pageSize;
    const newTableQuery = `
    SELECT
        platform, 
        platform_date, 
        twitter_id AS platform_id, 
        space_user,
        space_user_name,
        space_user_id,
        space_user_image,
        space_title,
        space_id,
        space_date
    FROM feed_item
    LEFT JOIN twitter_space ON feed_item.twitter_id = twitter_space.space_id
    WHERE platform = 'twitter'
    ORDER BY platform_date DESC
    LIMIT $1
    OFFSET $2;
    `;
    const totalItems = `SELECT COUNT(*) AS "feedCount" FROM feed_item;`;
    const newTableResult = await pool.query(newTableQuery, [pageSize, offset]);
    const { rows } = await pool.query(totalItems);
    res.json({ items: newTableResult.rows, count: rows[0].feedCount });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

router.get("/blog", async (req, res) => {
  try {
    const { page = 1, pageSize = 10 } = req.query;
    const offset = (page - 1) * pageSize;
    const newTableQuery = `
    SELECT
        platform, 
        platform_date, 
        blog_id AS platform_id,
        article_publisher,
        article_website,
        article_id,
        article_title,
        article_text,
        article_link,
        article_date
    FROM feed_item
    LEFT JOIN blog ON feed_item.blog_id = blog.article_id
    WHERE platform = 'blog'
    ORDER BY platform_date DESC
    LIMIT $1
    OFFSET $2;
    `;
    const totalItems = `SELECT COUNT(*) AS "feedCount" FROM feed_item;`;
    const newTableResult = await pool.query(newTableQuery, [pageSize, offset]);
    const { rows } = await pool.query(totalItems);
    res.json({ items: newTableResult.rows, count: rows[0].feedCount });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

router.get("/all", async (req, res) => {
  try {
    const { page = 1, pageSize = 10 } = req.query;
    const offset = (page - 1) * pageSize;
    const newTableQuery = `
    SELECT
        platform, 
        platform_date, 
        COALESCE(twitter_id, youtube_id, discord_id) AS platform_id, 
        message_project,
        message_id,
        message_channel_id,
        message_server_id,
        message_date,
        message_text,
        video_user,
        video_channel_title,
        video_user_image,
        video_title,
        video_description,
        video_id,
        space_user,
        space_user_name,
        space_user_id,
        space_user_image,
        space_title,
        space_id,
        space_date,
        article_publisher,
        article_website,
        article_id,
        article_title,
        article_text,
        article_link
    FROM feed_item
    LEFT JOIN twitter_space ON feed_item.twitter_id = twitter_space.space_id
    LEFT JOIN discord ON feed_item.discord_id = discord.message_id
    LEFT JOIN youtube ON feed_item.youtube_id = youtube.video_id
    LEFT JOIN blog ON feed_item.blog_id = blog.article_id
    ORDER BY platform_date DESC
    LIMIT $1
    OFFSET $2;
    `;
    const totalItems = `SELECT COUNT(*) AS "feedCount" FROM feed_item;`;
    const newTableResult = await pool.query(newTableQuery, [pageSize, offset]);
    const { rows } = await pool.query(totalItems);
    res.json({ items: newTableResult.rows, count: rows[0].feedCount });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Internal server error" });
  }
});

module.exports = router;
