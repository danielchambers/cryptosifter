const express = require("express");
const { getCurrentEpoch } = require("../utils/blockfrost");
const router = express.Router();

router.get("/current", async (req, res) => {
  try {
    const accountInfo = await getCurrentEpoch();
    res.json(accountInfo);
  } catch (error) {
    console.error(error);
    res.status(500).send("Failed to retrieve account info");
  }
});

module.exports = router;
