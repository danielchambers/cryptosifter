const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));

const BASE_JPGSTORE_URL = "https://server.jpgstoreapis.com";
const COLLECTION_API = `${BASE_JPGSTORE_URL}/collection`;
const USER_API = `${BASE_JPGSTORE_URL}/user`;

async function fetchJpgstore(url) {
  const response = await fetch(url, {
    headers: {
      "user-agent":
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36",
    },
  });
  if (!response.ok) {
    throw new Error(`Failed to fetch account info: ${response.statusText}`);
  }
  return response.json();
}

function getNftFloor(policyId) {
  return fetchJpgstore(`${COLLECTION_API}/${policyId}/floor`);
}

function getNftData(stakeAddress) {
  return fetchJpgstore(`${USER_API}/${stakeAddress}/data`);
}

module.exports = {
  // NFT
  getNftFloor,
  getNftData,
};
