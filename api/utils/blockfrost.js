const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));

const BLOCKFROST_API_KEY = process.env.BLOCKFROST_API_KEY;
const BLOCKFROST_ACCOUNTS_API = "https://cardano-mainnet.blockfrost.io/api/v0";

async function fetchBlockfrost(url) {
  const response = await fetch(url, {
    headers: {
      project_id: BLOCKFROST_API_KEY,
    },
  });
  if (!response.ok) {
    console.log("ERROR FETCH", response);
    throw new Error(`Failed to fetch account info: ${response.statusText}`);
  }
  return response.json();
}

// pool
function getCurrentStakedPool(poolId) {
  return fetchBlockfrost(`${BLOCKFROST_ACCOUNTS_API}/pools/${poolId}`);
}

function getPoolMetadata(poolId) {
  return fetchBlockfrost(`${BLOCKFROST_ACCOUNTS_API}/pools/${poolId}/metadata`);
}

// epoch
function getCurrentEpoch() {
  return fetchBlockfrost(`${BLOCKFROST_ACCOUNTS_API}/epochs/latest`);
}

// assets
function getAddressInfo(accountId) {
  return fetchBlockfrost(`${BLOCKFROST_ACCOUNTS_API}/accounts/${accountId}`);
}

function getAddressRewards(accountId) {
  return fetchBlockfrost(
    `${BLOCKFROST_ACCOUNTS_API}/accounts/${accountId}/rewards`
  );
}

function getAddressAssets(accountId) {
  return fetchBlockfrost(
    `${BLOCKFROST_ACCOUNTS_API}/accounts/${accountId}/addresses/assets`
  );
}

function getAddressDelegation(accountId) {
  return fetchBlockfrost(
    `${BLOCKFROST_ACCOUNTS_API}/accounts/${accountId}/history`
  );
}

function getAsset(asset) {
  return fetchBlockfrost(`${BLOCKFROST_ACCOUNTS_API}/assets/${asset}`);
}

module.exports = {
  // pool
  getCurrentStakedPool,
  getPoolMetadata,
  // epoch
  getCurrentEpoch,
  // assets
  getAddressInfo,
  getAddressRewards,
  getAddressAssets,
  getAddressDelegation,
  getAsset,
};
