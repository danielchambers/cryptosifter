const fetch = (...args) =>
  import("node-fetch").then(({ default: fetch }) => fetch(...args));

const GOMAESTRO_API_KEY = process.env.GOMAESTRO_API_KEY;
const GOMAESTRO_BASE_URL = "https://mainnet.gomaestro-api.org";
const ACCOUNTS_API = `${GOMAESTRO_BASE_URL}/accounts`;
const ASSETS_API = `${GOMAESTRO_BASE_URL}/assets`;

async function fetchGomaestro(url) {
  const response = await fetch(url, {
    headers: {
      accept: "application/json",
      "api-key": GOMAESTRO_API_KEY,
    },
  });

  if (!response.ok) {
    throw new Error(`Failed to fetch account info: ${response.statusText}`);
  }
  return response.json();
}

// account
function getAccountAssets(stakeAddress, page) {
  const params = new URLSearchParams();

  params.append("count", "100");
  params.append("page", page);
  return fetchGomaestro(
    `${ACCOUNTS_API}/${stakeAddress}/assets?${params.toString()}`
  );
}

// assets
function getAsset(asset) {
  return fetchGomaestro(`${ASSETS_API}/${asset}`);
}

module.exports = {
  // account
  getAccountAssets,
  //   assets
  getAsset,
};
