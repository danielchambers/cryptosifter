-- -- list \l
-- -- connect \c <DATABASE>
-- -- list tables \dt
-- -- psql -U test_user -d cryptosift

-- -- create user and password
-- CREATE USER test_user WITH PASSWORD 'HYgTfg567845DfwDHNihed36845$#@';

-- -- Create database
-- CREATE DATABASE cryptosift;


-- GRANT ALL PRIVILEGES ON DATABASE cryptosift TO test_user;
-- GRANT SELECT, INSERT, UPDATE, DELETE ON feed_item,twitter_space,youtube,discord,blog,price TO test_user;

-- -- Drop tables
-- DROP TABLE discord;
-- DROP TABLE feed_item,discord,youtube,twitter_space,blog;

-- -- Show data for table
-- SELECT * FROM discord;

-- -- create discord table
-- CREATE TABLE discord (
--     id SERIAL PRIMARY KEY,
--     message_project VARCHAR(100) NOT NULL,
--     message_text VARCHAR(10000) NOT NULL,
--     item_id VARCHAR(100) NOT NULL UNIQUE,
--     message_date VARCHAR(100) NOT NULL
-- );

-- -- create youtube table
-- CREATE TABLE youtube (
--     id SERIAL PRIMARY KEY,
--     video_user VARCHAR(100) NOT NULL,
--     video_title VARCHAR(500) NOT NULL,
--     video_description VARCHAR(10000) NOT NULL,
--     item_id VARCHAR(100) NOT NULL UNIQUE,
--     video_date VARCHAR(100) NOT NULL
-- );

-- -- create twitter space table
-- CREATE TABLE twitter_space (
--     id SERIAL PRIMARY KEY,
--     space_user VARCHAR(100) NOT NULL,
--     space_title VARCHAR(500) NOT NULL,
--     item_id VARCHAR(100) NOT NULL UNIQUE,
--     space_date VARCHAR(100) NOT NULL
-- );

-- -- create feed table
-- CREATE TABLE feed (
--     id SERIAL PRIMARY KEY,
--     platform VARCHAR(100) NOT NULL,
--     platform_date VARCHAR(100) NOT NULL,
--     item_id VARCHAR(100) NOT NULL,
--     CONSTRAINT fk_discord_item_id FOREIGN KEY (item_id) REFERENCES discord(item_id),
--     CONSTRAINT fk_youtube_item_id FOREIGN KEY (item_id) REFERENCES youtube(item_id),
--     CONSTRAINT fk_twitter_space_item_id FOREIGN KEY (item_id) REFERENCES twitter_space(item_id),
--     CONSTRAINT fk_youtube_item_id FOREIGN KEY (item_id) REFERENCES blog(item_id)
-- );


CREATE TABLE price (
    id SERIAL PRIMARY KEY,
    price FLOAT NOT NULL,
    policy_id VARCHAR(200) NOT NULL UNIQUE
);

CREATE TABLE discord (
    id SERIAL PRIMARY KEY,
    message_project VARCHAR(50) NOT NULL,
    message_text VARCHAR(5000) NOT NULL,
    message_id VARCHAR(50) NOT NULL UNIQUE,
    message_channel_id VARCHAR(50) NOT NULL,
    message_server_id VARCHAR(50) NOT NULL,
    message_date VARCHAR(50) NOT NULL
);

CREATE TABLE youtube (
    id SERIAL PRIMARY KEY,
    video_user VARCHAR(100) NOT NULL,
    video_channel_title VARCHAR(100) NOT NULL,
    video_user_image VARCHAR(200) NOT NULL,
    video_title VARCHAR(500) NOT NULL,
    video_description VARCHAR(5000) NOT NULL,
    video_id VARCHAR(100) NOT NULL UNIQUE,
    video_date VARCHAR(50) NOT NULL,
    video_thumbnail VARCHAR(200) NOT NULL
);

CREATE TABLE twitter_space (
    id SERIAL PRIMARY KEY,
    space_user VARCHAR(100) NOT NULL,
    space_user_name VARCHAR(200) NOT NULL,
    space_user_id VARCHAR(100) NOT NULL,
    space_user_image VARCHAR(200) NOT NULL,
    space_title VARCHAR(500) NOT NULL,
    space_id VARCHAR(100) NOT NULL UNIQUE,
    space_date VARCHAR(50) NOT NULL
);

CREATE TABLE blog (
    id SERIAL PRIMARY KEY,
    article_publisher VARCHAR(100) NOT NULL,
    article_website VARCHAR(100) NOT NULL,
    article_id VARCHAR(100) NOT NULL UNIQUE,
    article_title VARCHAR(500) NOT NULL,
    article_text VARCHAR(9000) NOT NULL,
    article_link VARCHAR(500) NOT NULL,
    article_date VARCHAR(50) NOT NULL
);

CREATE TABLE feed_item (
    id SERIAL PRIMARY KEY,
    platform VARCHAR(50) NOT NULL,
    platform_date VARCHAR(50) NOT NULL,
    twitter_id VARCHAR(100) REFERENCES twitter_space(space_id),
    discord_id VARCHAR(100) REFERENCES discord(message_id),
    youtube_id VARCHAR(100) REFERENCES youtube(video_id),
    blog_id VARCHAR(100) REFERENCES blog(article_id)
);