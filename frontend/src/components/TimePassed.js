import useTimePassed from "../hooks/useTimePassed";

function TimePassed({ timeStamp }) {
  const timePassed = useTimePassed(timeStamp);

  return <div>{timePassed}</div>;
}

export default TimePassed;
