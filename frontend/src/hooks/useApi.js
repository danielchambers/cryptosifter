import { useState, useEffect } from "react";

function useData(url, pageSize = 20) {
  const [data, setData] = useState([]);
  const [pageNumber, setPageNumber] = useState(1);

  useEffect(() => {
    setData([]);
    setPageNumber(1);
  }, [url]);

  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        `${url}?page=${pageNumber}&pageSize=${pageSize}`
      );
      const json = await response.json();
      setData((prevData) => [...prevData, ...json.items]);
    }
    fetchData();
  }, [url, pageNumber, pageSize]);

  return { data, setPageNumber };
}

export default useData;
