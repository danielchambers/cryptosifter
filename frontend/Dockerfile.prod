# Use an official Node.js runtime as a base image
FROM node:14 as build

# Set the working directory in the container
WORKDIR /app

# Copy the package.json and package-lock.json files to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy all frontend app files to the container
COPY . .

# Build the production version of the app
RUN npm run build

# Use a lightweight Node.js image for the production environment
FROM node:14-slim

# Set the working directory in the container
WORKDIR /app

# Copy the built frontend app from the build stage
COPY --from=build /app/build ./build

# Install a basic HTTP server to serve the frontend app
RUN npm install -g serve

# Expose port 3000 to access the frontend app
EXPOSE 3000

# Command to start the server and serve the frontend app
CMD ["serve", "-s", "build", "-p", "3000"]
