/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.html", "./src/**/*.js"],
  theme: {
    extend: {
      colors: {
        discord: "#5865f2",
        youtube: "#FF0000",
        twitter: "#1DA1F2",
      },
    },
  },
  plugins: [],
};
