# Cryptosifter

Cryptosifter is a comprehensive Crypto News Aggregator and token portfolio specifically designed for the Cardano blockchain ecosystem. The project's primary objective is to provide users with a curated selection of up-to-date and relevant news content from diverse sources within the Cardano community. Additionally, it offers a breakdown of token allocation within wallets, allowing users to easily track and manage their Cardano-based investments.

### Technologies Used

- **Python**: The primary programming language used for the development of Cryptosifter's backend logic, web scraping, and data processing.

- **Celery**: A distributed task queue used in conjunction with Redis as the message broker for processing asynchronous tasks. It gathers news articles, Twitter spaces, YouTube content, Discord messages, and token prices.

- **Tornado**: A Python web framework utilizing sockets for sending real-time updates to the frontend when new data is made available after processing the asynchronous tasks.

- **React**: A JavaScript library, along with the Tailwind CSS framework, is employed for the frontend.

- **ExpressJS**: A Javascript framework to create a RESTful API for retrieving the aggregated data from various tasks in a PostgreSQL database.

- **PostgreSQL**: Used to store and manage the aggregated data from the APIs, RSS feeds, and scraped data.

- **Docker**: A containerization platform to package and deploy Cryptosifter and its dependencies in a consistent and isolated environment.

- **Nginx**: A web server, reverse proxy and routing for Cryptosifter's backend services.

- **Web Scraping**: Cryptosifter utilizes the Python requests library for downloading blog RSS feeds, decentralized exchange token prices, and other relevant web content. The lxml library is employed for parsing and extracting necessary information from the downloaded web pages.

- **API Integration**: Cryptosifter integrates with various APIs to access data from external sources. This includes the Coingecko API for token prices, Blockfrost and Gomaestro for on-chain token metadata, Twitter for spaces data, YouTube for video data, and Discord for project announcement data.

## Project Architecture

![image](https://gitlab.com/danielchambers/cryptosifter/-/raw/main/Diagram.png)
