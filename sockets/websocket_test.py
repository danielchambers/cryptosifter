import os
import json
import time
import uuid
import random
import asyncio
import websockets


async def send_message(message):
    # 'ws://localhost:8888/websocket'
    async with websockets.connect('wss://localhost:8888/websocket') as websocket:
        await websocket.send(message)


def data_id():
    return uuid.uuid4().hex


def random_number(length):
    num = ''.join([str(random.randint(0, 9)) for _ in range(length)])
    return num


def ipsum(word_count):
    words = ['lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit', 'sed',
             'do', 'eiusmod', 'tempor', 'incididunt', 'ut', 'labore', 'et', 'dolore', 'magna', 'aliqua']
    sentence = ' '.join(random.sample(words, word_count))
    return sentence


if __name__ == '__main__':
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    item_id = data_id()
    item_date = int(time.time())

    discord = {
        # general
        'platform': "discord",
        'platform_date': item_date,
        'platform_id': item_id,
        # discord
        "project": ipsum(1),
        "message_text": ipsum(15),
        "message_id": item_id,
        "message_channel_id": data_id(),
        "message_server_id": data_id(),
        "message_date": item_date,
        'message_project': ipsum(1)
    }

    youtube = {
        # general
        'platform': "youtube",
        'platform_date': item_date,
        'platform_id': item_id,
        # youtube
        "video_description": ipsum(15),
        "video_id": item_id,
        "video_title": ipsum(15),
        "video_user": ipsum(2),
        'video_channel_title': ipsum(4),
        'video_user_image': 'http://www.someimage.com/test.jpg',
    }

    twitter = {
        # general
        "platform": "twitter",
        'platform_date': item_date,
        'platform_id': item_id,
        # twitter
        "space_id": item_id,
        "space_title": ipsum(10),
        "space_user": ipsum(1),
        'space_user_name': ipsum(1),
        'space_user_id': random_number(9),
        'space_user_image': 'http://www.someimage.com/test.jpg',
        'space_date': item_date
    }

    # message = json.dumps(twitter)
    # message = json.dumps(youtube)
    message = json.dumps(discord)
    asyncio.run(send_message(message))
