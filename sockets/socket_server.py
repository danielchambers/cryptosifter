import tornado.ioloop
import tornado.web
import tornado.websocket
from tornado.options import define, options

define("port", default=8888, help="run on the given port", type=int)
define("cookie_secret", default="put your cookie secret here",
       help="cookie secret for secure cookies")


class WebSocketHandler(tornado.websocket.WebSocketHandler):
    clients = set()

    def check_origin(self, origin):
        # define url in production
        return True

    def open(self):
        self.clients.add(self)

    def on_message(self, message):
        # print('>>>>', message)
        # data goes to client
        for client in self.clients:
            # data from the client
            # print('>>>>', client, message)
            client.write_message(message)

    def on_close(self):
        self.clients.remove(self)


def make_app(debug=False, autoreload=False):
    return tornado.web.Application([
        (r'/websocket', WebSocketHandler),
    ],
        debug=debug,
        autoreload=autoreload,
        cookie_secret=options.cookie_secret
    )


if __name__ == '__main__':
    app = make_app(debug=True, autoreload=True)
    port = options.port
    app.listen(port)
    print(f'Listening on port {port}')
    tornado.ioloop.IOLoop.current().start()
