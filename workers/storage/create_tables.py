from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import Column, Integer, Float, String, ForeignKey

# create engine
username = 'test_user'
password = 'PASSWORD'
engine = create_engine(
    f"postgresql://{username}:{password}@localhost:5432/cryptosift")
# engine = create_engine("postgresql://localhost/cryptosift")

# create declarative base
Base = declarative_base()

# # define models


class Discord(Base):
    __tablename__ = 'discord'
    id = Column(Integer, primary_key=True)
    message_project = Column(String(50), nullable=False)
    message_text = Column(String(5000), nullable=False)
    message_id = Column(String(50), nullable=False, unique=True)
    message_channel_id = Column(String(50), nullable=False)
    message_server_id = Column(String(50), nullable=False)
    message_date = Column(String(50), nullable=False)


class Youtube(Base):
    __tablename__ = 'youtube'
    id = Column(Integer, primary_key=True)
    video_user = Column(String(100), nullable=False)
    video_channel_title = Column(String(100), nullable=False)
    video_user_image = Column(String(200), nullable=False)
    video_title = Column(String(500), nullable=False)
    video_description = Column(String(5000), nullable=False)
    video_id = Column(String(100), nullable=False, unique=True)
    video_date = Column(String(50), nullable=False)
    video_thumbnail = Column(String(200), nullable=False)


class TwitterSpace(Base):
    __tablename__ = 'twitter_space'
    id = Column(Integer, primary_key=True)
    space_user = Column(String(100), nullable=False)
    space_user_name = Column(String(200), nullable=False)
    space_user_id = Column(String(100), nullable=False)
    space_user_image = Column(String(200), nullable=False)
    space_title = Column(String(500), nullable=False)
    space_id = Column(String(100), nullable=False, unique=True)
    space_date = Column(String(50), nullable=False)


class Blog(Base):
    __tablename__ = 'blog'
    id = Column(Integer, primary_key=True)
    article_publisher = Column(String(100), nullable=False)
    article_website = Column(String(100), nullable=False)
    article_id = Column(String(100), nullable=False, unique=True)
    article_title = Column(String(500), nullable=False)
    article_text = Column(String(9000), nullable=False)
    article_link = Column(String(500), nullable=False)
    article_date = Column(String(50), nullable=False)


class FeedItem(Base):
    __tablename__ = 'feed_item'
    id = Column(Integer, primary_key=True)
    platform = Column(String(50), nullable=False)
    platform_date = Column(String(50), nullable=False)
    twitter_id = Column(String(100), ForeignKey('twitter_space.space_id'))
    twitter = relationship("TwitterSpace", uselist=False)
    discord_id = Column(String(100), ForeignKey('discord.message_id'))
    discord = relationship("Discord", uselist=False)
    youtube_id = Column(String(100), ForeignKey('youtube.video_id'))
    youtube = relationship("Youtube", uselist=False)
    blog_id = Column(String(100), ForeignKey('blog.article_id'))
    blog = relationship("Blog", uselist=False)


class Price(Base):
    __tablename__ = 'price'
    id = Column(Integer, primary_key=True)
    price = Column(Float, nullable=False)
    policy_id = Column(String(200), nullable=False, unique=True)


# drop tables in db
Base.metadata.drop_all(engine)
# create tables in db
Base.metadata.create_all(engine)
