from sqlalchemy import exists
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import IntegrityError
from sqlalchemy import Table, MetaData
from storage.models import Price


def file_exists(session, model, record_id):
    does_file_exists = False
    if session.query(exists().where(model == record_id)).scalar():
        does_file_exists = True
    return does_file_exists


def save_data(session, model):
    try:
        session.add(model)
        session.commit()
    except IntegrityError as e:
        session.rollback()
    finally:
        session.close()


def merge_data(session, model):
    try:
        row = session.query(Price).filter_by(policy_id=model.policy_id).first()
        if row:
            row.price = model.price
        else:
            new_row = Price(price=model.price, policy_id=model.policy_id)
            session.add(new_row)
        session.commit()
    except IntegrityError as e:
        session.rollback()
    finally:
        session.close()


# def save_multiple_data(engine, models):
#     try:
#         Session = sessionmaker(bind=engine)
#         session = Session()
#         for model in models:
#             session.add(model)
#         session.commit()
#     except IntegrityError as e:
#         session.rollback()
#     session.close()
