import requests
from typing import List, Dict, Any, Union, Optional


class MaestroAPI:
    """
    A class representing a client for the GoMaestro API.

    Args:
        api_key (str): The API key to use for authentication.
        stake_address (str): The stake address of the Cardano account to retrieve data for.
        base_url (str, optional): The base URL of the Maestro API. Defaults to 'https://mainnet.gomaestro-api.org'.

    Attributes:
        api_key (str): The API key used for authentication.
        stake_address (str): The stake address of the Cardano account to retrieve data for.
        base_url (str): The base URL of the Maestro API.

    Methods:
        get_account(): Retrieves data for the Cardano account associated with the given stake address.
        get_assets(count, page): Retrieves data for the assets held by the Cardano account associated with the given stake address.
        get_staking_rewards(count, page): Retrieves data for the staking rewards earned by the Cardano account associated with the given stake address.
        get_pool_metadata(pool_id): Retrieves metadata for the specified Cardano stake pool.
        get_pool_information(pool_id): Retrieves information for the specified Cardano stake pool.

    Raises:
        Exception: If any request to the Maestro API fails with an HTTP error.
    """

    def __init__(self, api_key: str, stake_address: str, base_url: str = 'https://mainnet.gomaestro-api.org') -> None:
        """
        Initializes a new instance of the MaestroAPI class.

        Args:
            api_key (str): The API key to use for authentication.
            stake_address (str): The stake address associated with the account.
            base_url (str, optional): The base URL of the Maestro API. Defaults to 'https://mainnet.gomaestro-api.org'.
        """
        self.api_key = api_key
        self.stake_address = stake_address
        self.base_url = base_url

    def _make_request(self, endpoint: str, params: Dict[str, Any] = {}) -> Union[Dict[str, Any], List[Dict[str, Any]]]:
        """
        Sends a GET request to the specified endpoint with optional query parameters.

        Args:
            endpoint (str): The endpoint to request.
            params (Dict[str, Any], optional): A dictionary of query parameters. Defaults to {}.

        Returns:
            Union[Dict[str, Any], List[Dict[str, Any]]]: The response data as a dictionary or list of dictionaries.

        Raises:
            HTTPError: If the HTTP request fails, raises an HTTPError with the response status code.
        """
        headers = {'api-key': self.api_key}
        url = f'{self.base_url}{endpoint}'
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()
        data = response.json()
        return data

    def get_account(self) -> Dict[str, Any]:
        endpoint = f'/accounts/{self.stake_address}'
        try:
            data = self._make_request(endpoint)
            if isinstance(data, Dict):
                return data
            else:
                raise TypeError(
                    f"Expected Dict[str, Any] but got {type(data)}")
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e

    def get_assets(self, count: int, page: int) -> List[Dict[str, Any]]:
        """Get the account data for the configured stake address.

        Returns:
            Dict[str, Any]: The account data as a dictionary.

        Raises:
            TypeError: If the response data is not a dictionary.
            Exception: If the request failed with a non-200 HTTP status code.
        """
        endpoint = f'/accounts/{self.stake_address}/assets'
        params = {'count': count, 'page': page}
        try:
            data = self._make_request(endpoint, params=params)
            if isinstance(data, List):
                return data
            else:
                raise TypeError(
                    f"Expected List[Dict[str, Any]] but got {type(data)}")
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e

    def get_staking_rewards(self, count: int, page: int) -> List[Dict[str, Any]]:
        """
        Retrieves a list of staking rewards for the stake address associated with this Cardano client instance.

        Args:
            count (int): The number of items to include in the response.
            page (int): The page number to start the response from.

        Returns:
            List[Dict[str, Any]]: A list of staking rewards, where each item is represented as a dictionary containing
                information such as the amount of rewards earned, the date the rewards were earned, and the stake pool that
                generated the rewards.

        Raises:
            Exception: If the request to the Cardano API fails.
            TypeError: If the response data from the Cardano API is not in the expected format of a List of
                dictionaries.
        """
        endpoint = f'/accounts/{self.stake_address}/rewards'
        params = {'count': count, 'page': page}
        try:
            data = self._make_request(endpoint, params=params)
            if isinstance(data, List):
                return data
            else:
                raise TypeError(
                    f"Expected List[Dict[str, Any]] but got {type(data)}")
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e

    def get_pool_metadata(self, pool_id) -> Dict[str, Any]:
        """
        Retrieves metadata for the specified pool.

        Args:
            pool_id (str): ID of the pool to retrieve metadata for.

        Returns:
            dict: Dictionary containing metadata for the specified pool.

        Raises:
            Exception: If the HTTP request fails with a non-success status code.
            TypeError: If the returned data is not a dictionary.
        """
        endpoint = f'/pools/{pool_id}/metadata'
        try:
            data = self._make_request(endpoint)
            if isinstance(data, Dict):
                return data
            else:
                raise TypeError(
                    f"Expected Dict[str, Any] but got {type(data)}")
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e

    def get_pool_information(self, pool_id) -> Dict[str, Any]:
        """
        Retrieves information about the specified pool.

        Args:
            pool_id (str): ID of the pool to retrieve information for.

        Returns:
            dict: Dictionary containing information about the specified pool.

        Raises:
            Exception: If the HTTP request fails with a non-success status code.
            TypeError: If the returned data is not a dictionary.
        """
        endpoint = f'/pools/{pool_id}/info'
        try:
            data = self._make_request(endpoint)
            if isinstance(data, Dict):
                return data
            else:
                raise TypeError(
                    f"Expected Dict[str, Any] but got {type(data)}")
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e
