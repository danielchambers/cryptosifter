import requests
from typing import List, Dict, Any


class BlockfrostAPI:
    """
    A class representing a client for the Blockfrost API.

    Args:
        api_key (str): The API key for the Blockfrost API.
        stake_address (str): The stake address associated with the account.
        base_url (str, optional): The base URL for the Blockfrost API. Defaults to 'https://cardano-mainnet.blockfrost.io/api/v0'.

    Attributes:
        api_key (str): The API key for the Blockfrost API.
        stake_address (str): The stake address associated with the account.
        base_url (str): The base URL for the Blockfrost API.

    Methods:
        _make_request: Sends a GET request to the Blockfrost API.
        get_account: Retrieves the account information for the stake address associated with the client.
        get_asset: Retrieves the asset information for the specified asset ID.

    Raises:
        Exception: If the request to the API fails with an HTTP error.
    """

    def __init__(self, api_key: str, stake_address: str, base_url: str = 'https://cardano-mainnet.blockfrost.io/api/v0') -> None:
        """
        Initializes a new instance of the BlockfrostAPI class.

        Args:
            api_key (str): The API key for the Blockfrost API.
            stake_address (str): The stake address associated with the account.
            base_url (str, optional): The base URL for the Blockfrost API. Defaults to 'https://cardano-mainnet.blockfrost.io/api/v0'.
        """
        self.api_key = api_key
        self.stake_address = stake_address
        self.base_url = base_url

    def _make_request(self, endpoint: str, params: Dict[str, Any] = {}) -> List[Dict[str, Any]]:
        """
        Sends a GET request to the Blockfrost API.

        Args:
            endpoint (str): The endpoint to request.
            params (dict, optional): The query parameters to include in the request. Defaults to None.

        Returns:
            List[Dict[str, Any]]: The response data.

        Raises:
            Exception: If the request to the API fails with an HTTP error.
        """
        headers = {'project_id': self.api_key}
        url = f'{self.base_url}{endpoint}'
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()
        data = response.json()
        if isinstance(data, list):
            return data
        else:
            return [data]

    def get_account(self) -> Dict[str, Any]:
        """
        Retrieves the account information for the stake address associated with the client.

        Returns:
            Dict[str, Any]: The account information.

        Raises:
            Exception: If the request to the API fails with an HTTP error.
        """
        endpoint = f'/accounts/{self.stake_address}'
        try:
            response_data = self._make_request(endpoint)
            if response_data:
                return response_data[0]
            else:
                return {}
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e

    def get_asset(self, asset_id: str) -> List[Dict[str, Any]]:
        """
        Retrieves the asset information for the specified asset ID.

        Args:
            asset_id (str): The ID of the asset to retrieve.

        Returns:
            List[Dict[str, Any]]: The asset information.

        Raises:
            Exception: If the request to the API fails with an HTTP error.
        """
        endpoint = f'/assets/{asset_id}'
        try:
            return self._make_request(endpoint)
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e
