import requests
from typing import Dict, Any


class Jpgstore:
    def __init__(self, base_url: str = 'https://server.jpgstoreapis.com') -> None:
        self.base_url = base_url

    def _make_request(self, endpoint: str, params: Dict[str, Any] = {}) -> Dict[str, Any]:
        """
        Sends a GET request to the specified endpoint with optional query parameters.

        Args:
            endpoint (str): The endpoint to request.
            params (Dict[str, Any], optional): A dictionary of query parameters. Defaults to {}.

        Returns:
            Union[Dict[str, Any], List[Dict[str, Any]]]: The response data as a dictionary or list of dictionaries.

        Raises:
            HTTPError: If the HTTP request fails, raises an HTTPError with the response status code.
        """
        headers = {
            'user-agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36"}
        url = f'{self.base_url}{endpoint}'
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()
        data = response.json()
        return data

    def get_floor_price(self, policy_id: str) -> Dict[str, Any]:
        """Get the floor price for a given policy ID.

        Args:
            policy_id (str): The ID of the policy.

        Returns:
            Dict[str, Any]: A dictionary containing the floor price information.

        Raises:
            Exception: If the HTTP request fails with a non-success status code.
            TypeError: If the returned data is not a dictionary.
        """
        endpoint = f'/collection/{policy_id}/floor'
        try:
            data = self._make_request(endpoint)
            if isinstance(data, Dict):
                return data
            else:
                raise TypeError(
                    f"Expected Dict[str, Any] but got {type(data)}")
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e

    def get_wallet_data(self, stakeing_address: str) -> Dict[str, Any]:
        """Get the wallet data for a given staking address.

        Args:
            staking_address (str): The staking address to get wallet data for.

        Returns:
            Dict[str, Any]: A dictionary containing the wallet data.

        Raises:
            Exception: If the HTTP request fails with a non-success status code.
            TypeError: If the returned data is not a dictionary.
        """
        endpoint = f'/user/{stakeing_address}/data'
        try:
            data = self._make_request(endpoint)
            if isinstance(data, Dict):
                return data
            else:
                raise TypeError(
                    f"Expected Dict[str, Any] but got {type(data)}")
        except requests.exceptions.HTTPError as e:
            raise Exception(
                f'Request failed with status code {e.response.status_code}') from e
