import re
import os
import json
import asyncio
import websockets
from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from content.youtube import Youtube
from content.discord import Discord
from content.blog import Blog
from content.twitter.space import Space
from storage.database import file_exists, save_data
from storage.models import FeedItem
from storage.models import Youtube as YoutubeModel
from storage.models import TwitterSpace as TwitterSpaceModel
from storage.models import Discord as DiscordModel
from storage.models import Blog as BlogModel


async def send_message(message):
    ws_url = os.environ.get('TORNADO_URL')
    async with websockets.connect(ws_url) as websocket:
        await websocket.send(message)


def youtube(session):
    key = os.environ.get('YOUTUBE_KEY')
    youtube = Youtube(key)
    videos = youtube.get_videos('cardano', 5)
    for video in videos:
        timestamp = datetime.fromisoformat(
            video['video_date'] + '+00:00').timestamp()
        youtube_model = YoutubeModel(video_user=video['video_user'], video_channel_title=video['video_channel_title'],
                                     video_user_image=video['video_user_image'], video_title=video['video_title'],
                                     video_description=video['video_description'], video_id=video['video_id'],
                                     video_date=timestamp, video_thumbnail=video['video_thumbnail'])
        feed_item = FeedItem(platform="youtube",
                             platform_date=timestamp, youtube=youtube_model)
        if not file_exists(session, YoutubeModel.video_id, video['video_id']):
            save_data(session, feed_item)
            asyncio.run(send_message(json.dumps(video)))


def discord(session):
    key = os.environ.get('DISCORD_KEY')
    projects = {
        # server, channel
        'spectrum': ['881237412628533258', '920671212328341545'],
        'nunet': ['1033727051158536242', '1033797261710327821'],
        'adosia': ['907677998633844756', '978090325606277130'],
        'paribus': ['992492730339037295', '992556507403137136'],
        'iog': ['826816523368005654', '862918105835110400'],
        'eternl': ['907178289263681566', '907178289263681569'],
        'jpgstore': ['907023193942917222', '907057882619596820'],
        'dcspark': ['857911582972968981', '879812562026364968'],
        'tosidrop': ['968124541534601266', '976840237777051728'],
        'dripdropz': ['917849487794397254', '917850267972669450'],
        'singularitynet': ['909843832491896832', '912304954398638100'],
        'adao': ['881538386857451560', '927120621677191198'],
        'summon': ['881538386857451560', '999085853680869486'],
        'optim': ['885529372797313096', '900213682263441418'],
        'geniusx': ['944483563116564491', '945010233254764584'],
        'geniusyield': ['851910000003645490', '900479850358374410'],
        'axo': ['843173927831470170', '843812249898450987'],
        'anetabtc': ['876475955701501962', '876489352702722078'],
        'indiego': ['816779565796032513', '834798371872047125'],
        'minswap': ['829060079248343090', '832170039578853396'],
        'wingriders': ['915937361286795334', '915937634143043694'],
        'sundaeswap': ['830939534174453800', '830948250298875905'],
        'occamx': ['915703744178573372', '958052688988360734'],
        'occamfi': ['834847641463947364', '839862174334582845'],
        'aada': ['853921427286654987', '853921427875168258'],
        'meld general': ['850372362033430539', '860121140827127848'],
        'meld discord': ['850372362033430539', '916787377593274379'],
        'liqwid': ['759807412688388136', '981259898551664650'],
        'cornucopias': ['829374949587419137', '842583414439542805'],
        'pavia': ['897161313120321536', '897682883471347763'],
        'iamx': ['889780043776163872', '920396696117202964'],
        'iagon': ['837215135999197246', '846654998096117791']
    }
    discord = Discord(key)
    for project_name in projects:
        messages = discord.get_messages(
            project_name, projects[project_name], '5')
        for message in messages:
            timestamp = datetime.fromisoformat(
                message['message_date']).timestamp()
            discord_model = DiscordModel(message_project=message['message_project'], message_text=message['message_text'],
                                         message_id=message['message_id'], message_channel_id=message['message_channel_id'],
                                         message_server_id=message['message_server_id'], message_date=timestamp)
            feed_team = FeedItem(platform="discord",
                                 platform_date=timestamp, discord=discord_model)

            if not file_exists(session, DiscordModel.message_id, message['message_id']):
                save_data(session, feed_team)
                asyncio.run(send_message(json.dumps(message)))


def blogs(session):
    websites = [
        # None (DONE)
        {'name': 'utoday', 'url': 'https://u.today/rss'},
        # {'name': 'cryptobriefing', 'url': 'https://cryptobriefing.com/business/feed/'},
        # {'name': 'cryptobriefing', 'url': 'https://cryptobriefing.com/markets/feed/'},
        # {'name': 'cryptobriefing', 'url': 'https://cryptobriefing.com/analysis/feed/'},
        # {'name': 'cryptobriefing', 'url': 'https://cryptobriefing.com/technology/feed/'},
        # {'name': 'cryptobriefing', 'url': 'https://cryptobriefing.com/briefings/feed/'},
        # {'name': 'cryptobriefing', 'url': 'https://cryptobriefing.com/markets/feed/'},
        # {'name': 'newsbtc', 'url': 'https://www.newsbtc.com/tag/cardano/feed/'},
        {'name': 'newsbtc', 'url': 'https://www.newsbtc.com/tag/ada/feed/'},
        # {'name': 'cryptopotato',
        #     'url': 'https://cryptopotato.com/tag/cardano-ada-price/feed/'},
        # {'name': 'cryptopotato', 'url': 'https://cryptopotato.com/tag/cardano/feed/'},
        # {'name': 'bitcoinist', 'url': 'https://bitcoinist.com/category/cardano/feed/'},
        # {'name': 'watcherguru', 'url': 'https://watcher.guru/news/category/cardano/feed'},
        # {'name': 'cryptonews', 'url': 'https://crypto.news/tag/cardano/feed/'},
        # {'name': 'cryptoglobe', 'url': 'https://www.cryptoglobe.com/rss/feed.xml'},

        # <p><p> (DONE)
        {'name': 'beincrypto', 'url': 'https://beincrypto.com/markets/feed/'},
        # {'name': 'beincrypto', 'url': 'https://beincrypto.com/technology/feed/'},
        # {'name': 'beincrypto', 'url': 'https://beincrypto.com/business/feed/'},
        # {'name': 'cryptoslate', 'url': 'https://cryptoslate.com/news/cardano/feed/'},
        # too large and encoding issues
        # {'name': 'analyticsinsight', 'url': 'https://www.analyticsinsight.net/category/cryptocurrency/feed/'},
        # {'name': 'analyticsinsight', 'url': 'https://www.analyticsinsight.net/category/latest-news/feed/'},

        # <p> (DONE)
        {'name': 'zycrypto', 'url': 'https://zycrypto.com/tag/cardano/feed/'},
        # {'name': 'zycrypto', 'url': 'https://zycrypto.com/tag/adausd/feed/'},
        # {'name': 'zycrypto', 'url': 'https://zycrypto.com/tag/ada/feed/'},

        # <img> (DONE)
        {'name': 'ambcrypto', 'url': 'https://ambcrypto.com/category/altcoins-news/feed/'},
        # {'name': 'forkastnews',
        #     'url': 'https://forkast.news/topic/cryptocurrencies/cardano-ada/feed/'},
        # {'name': 'bitcoincom', 'url': 'https://news.bitcoin.com/tag/cardano/feed/'},

        # <a><p><p> (DONE)
        {'name': 'dailycoin', 'url': 'https://dailycoin.com/tag/cardano-ada/feed/'},

        # <img><p> (DONE)
        {'name': 'dailyhodl', 'url': 'https://dailyhodl.com/altcoins/feed/'},


        # invalid rss (TODO)
        # {'name': 'cryptonewscom', 'url': 'https://cryptonews.com/news/altcoin-news/feed/'},
        # Needs custom parsing (TODO)
        # {'name': 'coindesk',
        #     'url': 'https://www.coindesk.com/arc/outboundfeeds/rss/?outputType=xml'},
        # {'name': 'cointelegraph', 'url': 'https://cointelegraph.com/rss/tag/ada'},
        # {'name': 'cointelegraph', 'url': 'https://cointelegraph.com/rss/tag/cardano'}

    ]
    blog = Blog()
    blogs = blog.get_posts(websites, ['cardano', 'ada', 'hoskinson', 'emergo'])

    # merge all dicts to a single list
    posts = []
    for blog_name in blogs:
        posts.extend(blogs[blog_name])

    for post in posts:
        blog_model = BlogModel(article_publisher=post['article_publisher'], article_website=post['article_website'],
                               article_id=post['article_id'], article_title=post['article_title'], article_text=post['article_text'],
                               article_link=post['article_link'], article_date=post['article_date'])
        feed_item = FeedItem(platform="blog",
                             platform_date=post['article_date'], blog=blog_model)

        if not file_exists(session, BlogModel.article_id, post['article_id']):
            save_data(session, feed_item)
            asyncio.run(send_message(json.dumps(post)))


def space(session):
    key = os.environ.get('TWITTER_KEY')
    space = Space(key)

    all_spaces = []

    # spaces that contain token ticker
    ticker = 'ada'
    ticker_spaces = space.search_keyword(ticker, 'live')
    for ticker_space in ticker_spaces:
        if "title" in ticker_space.keys():
            results = re.findall(
                f'[\#|\$]{ticker}', ticker_space['title'].lower())
            if results:
                timestamp = datetime.fromisoformat(
                    ticker_space['started_at'].replace('Z', '') + '+00:00').timestamp()
                all_spaces.append({
                    'space_user': ticker_space['creator_id']['username'],
                    'space_user_name': ticker_space['creator_id']['name'],
                    'space_user_id': ticker_space['creator_id']['id'],
                    'space_user_image': ticker_space['creator_id']['profile_image_url'],
                    'space_title': ticker_space['title'],
                    'space_id': ticker_space["id"],
                    'space_date': timestamp,
                })

    # spaces that contain keywords
    keywords = ['cardano', 'cnft']
    keyword_spaces = space.search_keywords(keywords, 'live')
    for ticker_space in keyword_spaces:
        timestamp = datetime.fromisoformat(
            ticker_space['started_at'].replace('Z', '') + '+00:00').timestamp()
        all_spaces.append({
            'space_user': ticker_space['creator_id']['username'],
            'space_user_name': ticker_space['creator_id']['name'],
            'space_user_id': ticker_space['creator_id']['id'],
            'space_user_image': ticker_space['creator_id']['profile_image_url'],
            'space_title': ticker_space['title'],
            'space_id': ticker_space["id"],
            'space_date': timestamp
        })

    # spaces based on users id
    user_spaces = space.search_creator_ids("cardano", "1376161898,986726837660205056,1450094001365917711,1425233298796597252,1378490063588257792,1451899813176426502,1411286394253754370,1420663296408031234,1371522443362242566,1447734876552286211,1490668220256337922,1502640824965812227,1447723217494294530,1431504317156970503,1383664618183094280,1395311347500527617,1397935352757772289,982253622514802688,1466810642359365638,1438693540150882305,1413777097504563202,1377800316561850369,1139618448412205059,1402023355696529416,1384492289964212225,1384821991971201028,1371725080275906565,928375322017382401,1384455565187891201,1462139155002048513,1438517225917325313,1302408526925631493,1405086308528185345,1486777331352055824,1454574462473363459,1453689655300329472,1458797692759461890,1388025220322234368,1497254745593069579,913327957904695297,1293818320387538944,1390323666601250818,1379269821880098819,1369617811568484352")
    for ticker_space in user_spaces:
        timestamp = datetime.fromisoformat(
            ticker_space['started_at'].replace('Z', '') + '+00:00').timestamp()
        all_spaces.append({
            'space_user': ticker_space['creator_id']['username'],
            'space_user_name': ticker_space['creator_id']['name'],
            'space_user_id': ticker_space['creator_id']['id'],
            'space_user_image': ticker_space['creator_id']['profile_image_url'],
            'space_title': ticker_space['title'],
            'space_id': ticker_space["id"],
            'space_date': timestamp
        })

    # filter spaces to prevent duplicates
    unique_spaces = []
    existing_ids = set()
    for s in all_spaces:
        if s['space_id'] not in existing_ids:
            unique_spaces.append(s)
            existing_ids.add(s['space_id'])

    # save twitter space in database
    for unique_space in unique_spaces:
        twitter_space = TwitterSpaceModel(space_user=unique_space['space_user'], space_user_name=unique_space['space_user_name'],
                                          space_user_id=unique_space['space_user_id'], space_user_image=unique_space['space_user_image'],
                                          space_title=unique_space['space_title'], space_id=unique_space['space_id'],
                                          space_date=unique_space['space_date'])
        feed_item = FeedItem(platform="twitter",
                             platform_date=unique_space['space_date'], twitter=twitter_space)

        if not file_exists(session, TwitterSpaceModel.space_id, unique_space['space_id']):
            save_data(session, feed_item)
            asyncio.run(send_message(json.dumps(unique_space)))


if __name__ == "__main__":
    # task to run
    action = 'youtube'
    # postgres
    postgres_url = os.environ.get('POSTGRES_DATABASE_URL')
    engine = create_engine(postgres_url)
    Session = sessionmaker(bind=engine)
    session = Session()
    # websocket
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)

    if action == 'youtube':
        youtube(session)
        print('saved youtube videos')
    if action == 'discord':
        discord(session)
        print('saved discord messages')
    if action == 'blogs':
        blogs(session)
        print('saved blog articles')
    if action == 'spaces':
        space(session)
        print('saved twitter space')
