import os
import requests
from functools import reduce
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from storage.database import merge_data
from storage.models import Price


def sum(a, b):
    return a + b


def coingecko():
    url = 'https://api.coingecko.com/api/v3/simple/price?ids=cardano,djed&vs_currencies=usd'
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
        "Content-Type": "application/json"
    }
    response = requests.get(url, headers=headers)
    data = response.json()
    return [
        {'policyid': '000', 'price': float(data['cardano']['usd'])},
        {'policyid': '8db269c3ec630e06ae29f74bc39edd1f87c819f1056206e879a1cd61',
            'price': float(data['djed']['usd'])}
    ]


def wingriders():
    url = "https://api.mainnet.wingriders.com/graphql"
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
        "Content-Type": "application/json"
    }
    payload = [
        {
            "operationName": "AssetsAdaExchangeRates",
            "variables": {},
            "query": "query AssetsAdaExchangeRates {\n  assetsAdaExchangeRates {\n    ...AssetExchangeRateFragment\n    __typename\n  }\n}\n\nfragment AssetExchangeRateFragment on AssetExchangeRate {\n  assetId\n  baseAssetId\n  exchangeRate\n  __typename\n}"
        },
        {
            "operationName": "AssetsPrices",
            "variables": {
                "input": [
                    {"policyId": "", "assetName": ""},
                    {"policyId": "8db269c3ec630e06ae29f74bc39edd1f87c819f1056206e879a1cd61",
                        "assetName": "446a65644d6963726f555344"},
                    {"policyId": "f66d78b4a3cb3d37afa0ec36461e51ecbde00f26c8f0a68f94b69880",
                        "assetName": "69555344"}
                ]
            },
            "query": "query AssetsPrices($input: [AssetOrAdaInput!]!) {\n  assetsPrices(input: $input) {\n    policyId\n    assetName\n    priceInUsd\n    __typename\n  }\n}"
        }
    ]
    response = requests.post(url, headers=headers, json=payload)
    data = response.json()[0]['data']['assetsAdaExchangeRates']
    return [{'policyid': token['assetId'], 'price': float(token['exchangeRate'])} for token in data]


def muesliswap():
    url = 'https://api.muesliswap.com/list?base-policy-id=&base-tokenname='
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
        "Content-Type": "application/json"
    }
    tokens = []
    response = requests.get(url, headers=headers)
    data = response.json()
    for token in data:
        new_data_dict = {
            "policyid": token["info"]["address"]["policyId"],
            "price": float(token["price"]["price"])
        }
        tokens.append(new_data_dict)
    return tokens


def sundaeswap(ada_price):
    url = "https://stats.sundaeswap.finance/graphql"
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
        "Content-Type": "application/json"
    }
    payload = {
        "query": "query getPopularPools($pageSize: Int) {\n  poolsPopular(pageSize: $pageSize) {\n    ...ExtendPoolFragment\n  }\n}\n\nfragment ExtendPoolFragment on Pool {\n  ...PoolFragment\n  ...PoolInfoFragment\n}\n\nfragment PoolFragment on Pool {\n  apr\n  rewards {\n    apr\n    asset {\n      ...AssetFragment\n    }\n  }\n  assetA {\n    ...AssetFragment\n  }\n  assetB {\n    ...AssetFragment\n  }\n  assetLP {\n    ...AssetFragment\n  }\n  fee\n  quantityA\n  quantityB\n  quantityLP\n  ident\n  assetID\n}\n\nfragment AssetFragment on Asset {\n  assetId\n  policyId\n  assetName\n  decimals\n  logo\n  ticker\n  dateListed\n  sources\n}\n\nfragment PoolInfoFragment on Pool {\n  tvl\n  name\n  priceUSD\n}",
        "variables": {},
        "operationName": "getPopularPools"
    }
    response = requests.post(url, json=payload, headers=headers)
    data = response.json()['data']['poolsPopular']
    return [{'policyid': token['assetB']['policyId'], 'price': float(token['priceUSD']) / ada_price} for token in data]


def get_prices():
    gecko = coingecko()
    m = muesliswap()
    s = sundaeswap(gecko[0]['price'])
    w = wingriders()

    combined_list = m + s + w + gecko
    final_list = {}
    postgres_url = os.environ.get('POSTGRES_URL')
    engine = create_engine(postgres_url)
    Session = sessionmaker(bind=engine)
    session = Session()

    for token in combined_list:
        if token['policyid']:
            token_id = token['policyid'][:56]
            if token_id in final_list.keys():
                final_list[token_id].append(token['price'])
            else:
                final_list[token_id] = [token['price']]

    for final_token in final_list:
        prices = final_list[final_token]
        total_price = reduce(sum, prices)
        if final_token == 'f43a62fdc3965df486de8a0d32fe800963589c41b38946602a0dc535':
            # adjust agix token price
            total_price = total_price * 100
        if final_token == 'b34b3ea80060ace9427bda98690a73d33840e27aaa8d6edb7f0c757a':
            # adjust cneta token price
            total_price = total_price / 1000000
        if final_token == "a0028f350aaabe0545fdcb56b039bfb08e4bb4d8c4d7c3c7d481c235":
            total_price = total_price / 1000000
        if final_token == "4247d5091db82330100904963ab8d0850976c80d3f1b927e052e07bd":
            total_price = total_price / 1000000

        total_items = len(prices)
        p = Price(price=(total_price / total_items), policy_id=final_token)
        merge_data(session, p)
