import os
import requests
import unittest
from unittest.mock import patch
from blockchain.gomaestro import MaestroAPI


class TestMaestroAPI(unittest.TestCase):
    def setUp(self):
        self.api_key = 'my_api_key'
        self.stake_address = 'my_stake_address'
        self.base_url = 'https://test.gomaestro-api.org'
        self.maestro_api = MaestroAPI(
            self.api_key, self.stake_address, self.base_url)

    @patch('requests.get')
    def test_get_account_success(self, mock_get):
        expected_response = {'account': {'balance': 100}}
        mock_get.return_value.json.return_value = expected_response
        response = self.maestro_api.get_account()

        self.assertEqual(response, expected_response)

    @patch('requests.get')
    def test_get_account_error(self, mock_get):
        mock_get.side_effect = requests.exceptions.HTTPError('404 Not Found')

        with self.assertRaises(Exception):
            self.maestro_api.get_account()
