import re
import os
import requests
from typing import List, Dict, Any


class Discord:
    def __init__(self, api_key: str, base_url: str = 'https://discord.com/api/v9') -> None:
        """
        A class for interacting with the Discord API.

        Args:
            api_key (str): The API key for the Discord API.
            base_url (str, optional): The base URL for the Discord API. Defaults to 'https://discord.com/api/v9'.

        Attributes:
            api_key (str): The API key for the Discord API.
            base_url (str): The base URL for the Discord API.

        Methods:
            _make_request(endpoint: str, params: Dict[str, Any] = {}) -> List[Dict[str, Any]]:
                Sends a GET request to the Discord API.

            replace_user_ids(text: str, users: Dict[str, str]) -> str:
                Replaces user IDs in the provided text with their respective usernames.

            get_messages(project_name: str, project_ids: List, messages_limit: str) -> List[Dict[str, Any]]:
                Gets messages from the specified channel and returns them in a list of dictionaries.
        """
        self.api_key = api_key
        self.base_url = base_url

    def _make_request(self, endpoint: str, params: Dict[str, Any] = {}) -> List[Dict[str, Any]]:
        """
        Sends a GET request to the Blockfrost API.

        Args:
            endpoint (str): The endpoint to request.
            params (dict, optional): The query parameters to include in the request. Defaults to None.

        Returns:
            List[Dict[str, Any]]: The response data.

        Raises:
            Exception: If the request to the API fails with an HTTP error.
        """
        headers = {'authorization': self.api_key}
        url = f'{self.base_url}{endpoint}'
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()
        data = response.json()
        if isinstance(data, list):
            return data
        else:
            return [data]

    def replace_user_ids(self, text: str, users: Dict[str, str]) -> str:
        """
        Replaces user IDs with usernames in a given text.

        Args:
            text (str): The text to replace user IDs in.
            users (Dict[str, str]): A dictionary of user IDs to usernames.

        Returns:
            str: The text with user IDs replaced by usernames.
        """
        pattern = r'<@(.*?)>'
        replaced_text = re.sub(pattern, lambda match: users.get(
            match.group(1), match.group(0)), text)
        return replaced_text

    def get_messages(self, project_name: str, project_ids: List[str], messages_limit: str) -> List[Dict[str, Any]]:
        """
        Gets messages from a Discord channel.

        Args:
            project_name (str): The name of the project.
            project_ids (List[str]): A list containing the server ID and channel ID.
            messages_limit (int): The maximum number of messages to retrieve.

        Returns:
            List[Dict[str, Any]]: A list of message data.

        Raises:
            Exception: If the request to the API fails with an HTTP error.
        """
        server_id, channel_id = project_ids
        all_messages = []
        endpoint = f'/channels/{channel_id}/messages?limit={messages_limit}'
        messages = self._make_request(endpoint)

        for message in messages:
            message_item = {
                'message_id': message['id'],
                'message_project': project_name,
                'message_text': message['content'],
                'message_channel_id': message['channel_id'],
                'message_server_id': server_id,
                'message_date': message['timestamp']
            }

            # replace user ids with username
            mentions = message['mentions']
            if mentions:
                users = {
                    mention['id']: f"@{mention['username']}" for mention in mentions}
                replaced_text = self.replace_user_ids(
                    message['content'], users)
                message_item['message_text'] = replaced_text

            # replace twitter link with tweet text
            embeds = message['embeds']
            if embeds and embeds[0]['type'] == 'rich':
                message_item['message_text'] = embeds[0]['description']

            # get attatchments
            attatchments = message['attachments']
            if attatchments:
                message_item['message_attachments'] = ','.join(
                    [attatchment['url'] for attatchment in attatchments])

            all_messages.append(message_item)
        return all_messages
