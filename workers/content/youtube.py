import os
from googleapiclient.discovery import build
from typing import Dict, List, Any


class Youtube():
    """
    A class that interacts with the YouTube API to search for videos by keyword and retrieve video/channel data.

    Args:
        key (str): The API key used to authenticate the API calls.

    Attributes:
        api: The YouTube API service object used to make API requests.

    Methods:
        search_by_keyword: Searches for videos on YouTube that match a given keyword and returns a dictionary of video results.
        get_channel_data: Retrieves data about a YouTube channel by its ID and returns a dictionary of channel information.
        get_video_data: Retrieves data about a YouTube video by its ID and returns a dictionary of video information.
        get_videos: Searches for videos on YouTube that match a given keyword and returns a list of dictionaries containing video data.

    """

    def __init__(self, key):
        """
        Initializes the YouTube API service object with the provided API key.

        Args:
            key (str): The API key used to authenticate the API calls.

        Returns:
            None

        """
        self.api = youtube = build('youtube', 'v3', developerKey=key)

    def search_by_keyword(self, youtube: object, term: str, limit: int) -> Dict[str, Any]:
        """
        Searches for videos on YouTube that match a given keyword and returns a dictionary of video results.

        Args:
            youtube (object): The YouTube API service object used to make API requests.
            term (str): The keyword to search for on YouTube.
            limit (int): The maximum number of video results to return.

        Returns:
            Dict[str, Any]: A dictionary containing the video results matching the provided keyword.

        """
        return self.api.search().list(q=term, part='id,snippet', maxResults=limit).execute()

    def get_channel_data(self, youtube: object, channel_id: str) -> Dict[str, Any]:
        """
        Retrieves data about a YouTube channel by its ID and returns a dictionary of channel information.

        Args:
            youtube (object): The YouTube API service object used to make API requests.
            channel_id (str): The ID of the YouTube channel to retrieve data for.

        Returns:
            Dict[str, Any]: A dictionary containing the data for the requested YouTube channel.

        """
        return self.api.channels().list(part='snippet', id=channel_id).execute()

    def get_video_data(self, youtube: object, video_id: str) -> Dict[str, Any]:
        """
        Retrieves data about a YouTube video by its ID and returns a dictionary of video information.

        Args:
            youtube (object): The YouTube API service object used to make API requests.
            video_id (str): The ID of the YouTube video to retrieve data for.

        Returns:
            Dict[str, Any]: A dictionary containing the data for the requested YouTube video.

        """
        return self.api.videos().list(part='snippet', id=video_id).execute()

    def get_videos(self, keyword: str, number_of_videos: int) -> List[Dict[str, Any]]:
        """
        Searches for videos on YouTube that match a given keyword and returns a list of dictionaries containing video data.

        Args:
            keyword (str): The keyword to search for on YouTube.
            number_of_videos (int): The maximum number of video results to return.

        Returns:
            List[Dict[str, Any]]: A list of dictionaries containing data for each video that matched the provided keyword.

        """
        video_list = []
        keyword_results = self.search_by_keyword(
            self.api, keyword, number_of_videos)
        videos = [video for video in keyword_results['items']
                  if 'videoId' in video['id'].keys()]
        for video in videos:
            channel_data = self.get_channel_data(
                self.api, video['snippet']['channelId'])
            video_id = video['id']['videoId']
            video_data = self.get_video_data(self.api, video_id)[
                'items'][0]['snippet']
            #
            if keyword in video_data['title'].lower():
                video_list.append({
                    'id': video_id,
                    'video_user': channel_data['items'][0]['snippet']['customUrl'],
                    'video_channel_title': video_data['channelTitle'],
                    'video_user_image': channel_data['items'][0]['snippet']['thumbnails']['high']['url'],
                    'video_title': video_data['title'],
                    'video_description': video_data['description'],
                    'video_id': video_id,
                    'video_date': video_data['publishedAt'],
                    'video_thumbnail': video_data['thumbnails']['high']['url']
                })
        return video_list
