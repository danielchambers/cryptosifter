import re
import time
import hashlib
import requests
from dateutil import parser as dateParser, tz
from lxml import etree, html
from typing import List, Dict, Any, Union


class Blog:
    def __init__(self) -> None:
        """
        A class for retrieving and parsing blog feeds from multiple URLs.

        Methods:
            _make_request(url: str, params: Dict[str, Any] = {}) -> bytes:
                Sends a GET request to the specified URL and returns the response content as bytes.

            build_tree(url: str) -> etree.Element:
                Sends a GET request to the specified URL, parses the response content as XML, and returns the root node of the resulting tree.

            sanatize_title(item: etree.Element) -> str:
                Extracts the sanitized title from an XML node representing a blog post and returns it as a string.

            sanatize_link(item: etree.Element) -> str:
                Extracts the sanitized link from an XML node representing a blog post and returns it as a string.

            sanatize_publish_date(item: etree.Element) -> float:
                Extracts the sanitized publish date from an XML node representing a blog post and returns it as a Unix timestamp.

            sanitize_content(item: etree.Element) -> str:
                Extracts the sanitized content from an XML node representing a blog post and returns it as a string.

            get_trees() -> List[Dict[str, Any]]:
                Retrieves the XML trees for each blog URL and returns them as a list of dictionaries, where each dictionary contains the name and root node of a blog's XML tree.

            get_posts(keywords: List[str]) -> Dict[str, List[Dict[str, Any]]]:
                Retrieves the blog posts from each blog's XML tree, filters the posts by the specified keywords, and returns them as a dictionary, where each key is a blog name and each value is a list of dictionaries representing blog posts.
        """

    def _make_request(self, url: str, params: Dict[str, Any] = {}) -> Union[bytes, None]:
        """
        Makes a GET request to the specified URL.

        Args:
            url (str): The URL to request.
            params (Dict[str, Any]): Optional parameters to include in the request.

        Returns:
            bytes: The content of the response.
        """
        try:
            headers = {
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36'}
            response = requests.get(
                url, headers=headers, params=params, timeout=15)
            response.raise_for_status()
            # print(response.text)
            data = response.content
            return data
        except requests.exceptions.Timeout:
            print("The request timed out")

    def build_tree(self, url: str) -> etree._Element:
        """
        Builds an XML tree from the specified URL.

        Args:
            url (str): The URL to build the tree from.

        Returns:
            etree.Element: The root element of the XML tree.
        """
        response = self._make_request(url)
        xml_parser = etree.XMLParser(huge_tree=True, recover=True)
        tree = etree.fromstring(response, parser=xml_parser)
        return tree

    def sanatize_title(self, item: etree._Element) -> str:
        """
        Extracts and sanitizes the title of the specified item.

        Args:
            item (etree.Element): The item to extract the title from.

        Returns:
            str: The sanitized title.
        """
        return item.xpath('title/text()')[0].strip()

    def sanatize_link(self, item: etree._Element) -> str:
        """
        Extracts and sanitizes the link of the specified item.

        Args:
            item (etree.Element): The item to extract the link from.

        Returns:
            str: The sanitized link.
        """
        return item.xpath('link/text()')[0]

    def sanatize_publish_date(self, item: etree._Element):
        """
        Extracts and sanitizes the publish date of the specified item.

        Args:
            item (etree.Element): The item to extract the publish date from.

        Returns:
            float: The Unix timestamp representing the publish date.
        """
        pubdate = item.xpath('pubDate/text()')[0]
        # define a timezone info object that maps "EDT" to UTC-4
        tzinfos = {'EDT': tz.gettz('UTC-4')}
        date_object = dateParser.parse(pubdate, tzinfos=tzinfos)
        timestamp = int(date_object.timestamp())
        return timestamp

    def sanitize_content(self, item: etree._Element) -> str:
        """
        Sanitizes the content of a blog post.

        Args:
            item: An lxml Element object representing the blog post.

        Returns:
            The sanitized content of the blog post as a string.
        """
        description = item.xpath("description/text()")[0]
        elements = html.fromstring(description)
        tags = ''.join([element.tag for element in elements])

        # parse based on tags matching <p>, <img>
        if tags in ['p', 'img', '']:
            p = elements.xpath("text()")[0]
            return p.replace('[…]', '...').strip()

        # parse based on tags matching <p><p>, <a><p><p>, <img><p><p>
        if tags in ['pp', 'app', 'imgpp']:
            p = elements.find(".//p")
            return p.text.replace('[…]', '...').strip()
        return ""

    def get_trees(self, urls: List[Dict[str, str]]) -> List[Dict[str, Any]]:
        """
        Retrieves XML trees for each URL in the blog's list of URLs.

        Returns:
            A list of dictionaries, where each dictionary contains a website name and its corresponding tree.
        """
        trees = []
        try:
            for website in urls:
                tree_data = {}
                tree_data['name'] = website['name']
                tree_data['tree'] = self.build_tree(website['url'])
                trees.append(tree_data)
        except ValueError:
            print('ERROR!!!!!!!!!!!!!!!!!!!')
            print('urls', urls)
        return trees

    def get_posts(self, urls: List[Dict[str, str]], keywords: List[str]) -> Dict[str, List[Dict[str, Any]]]:
        """
        Retrieves blog posts from each URL in the blog's list of URLs that contain the specified keywords.

        Args:
            urls (List[Dict[str, str]]): A list of dictionaries, where each dictionary contains the name and URL of a blog.
            keywords: A list of keywords to search for in the blog posts.

        Returns:
            A dictionary where each key is a website name and the value is a list of dictionaries,
            where each dictionary contains information about a matching blog post.
        """
        posts = {}
        for feed in self.get_trees(urls):
            feed_name = feed['name']
            tree = feed['tree']
            items = tree.xpath('//item')
            for item in items:
                title = self.sanatize_title(item)
                if set(title.lower().split(' ')).intersection(set(keywords)):
                    link = self.sanatize_link(item)
                    publish_date = self.sanatize_publish_date(item)
                    content = self.sanitize_content(item)
                    url_match = re.match(r"(https?://[^/]+)", link)
                    title_hash = hashlib.md5(title.encode('utf-8')).hexdigest()
                    data = {'article_publisher': feed_name, 'article_website': url_match.group(), 'article_title': title,
                            'article_link': link, 'article_date': publish_date, 'article_text': content,
                            'article_id': title_hash}
                    if feed_name in posts.keys():
                        posts[feed_name].append(data)
                    else:
                        posts[feed_name] = [data]
        return posts
