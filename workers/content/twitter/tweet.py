import re
from .http import Http


class Tweet(Http):
    """A wrapper for Twitter Tweets API."""

    base_url = 'https://api.twitter.com/2/tweets/'
    
    def search_tweet_id(self, tweet_id: str) -> list:
        """Retrieves data about a tweet given its id number.

        Args:
            tweet_id: A list of tweet ids seperated by comma.

        Returns:
            A list of objects containing tweet information.
        """

        url = f'{self.base_url}'
        params = {
            'ids': tweet_id,
            'expansions': 'attachments.media_keys',
            'media.fields': 'preview_image_url,url,alt_text',
            'tweet.fields': 'text,attachments'
        }
        response = self.get(url, params)
        if 'data' in response.json().keys():
            resp = response.json()
            return resp['data']
        else:
            return []