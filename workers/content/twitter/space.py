import re
from .http import Http


class Space(Http):
    """A wrapper for Twitter spaces API."""

    base_url = 'https://api.twitter.com/2/spaces'
    
    def search_keywords(self, keywords: list, state: str) -> list:
        """Retrieves data about spaces based on more than one keyword.

        Args:
            keywords: A list of strings to search for.
            state: The state of a space. Can be live, scheduled or ended.

        Returns:
            A list of objects containing space information.
        """

        spaces = []
        for keyword in keywords:
            spaces.extend(self.search_keyword(keyword, state))
        return spaces

    def search_keyword(self, keyword: str, state: str) -> list:
        """Retrieves data about spaces based on a keyword.

        Args:
            user_ids: A list of user ids seperated by comma.

        Returns:
            A list of objects containing space information.
        """

        url = f'{self.base_url}/search'
        params = {
            'query': keyword,
            'state': state,
            'expansions': 'invited_user_ids,speaker_ids,creator_id,host_ids',
            'space.fields': 'invited_user_ids,host_ids,created_at,creator_id,participant_count,speaker_ids,started_at,ended_at,title,scheduled_start,state',
            'user.fields': 'id,name,username,profile_image_url,public_metrics'
        }
        response = self.get(url, params)
        if 'data' in response.json().keys():
            resp = response.json()
            self.convert_user_id(resp)
            self.parse_keywords(resp, keyword=keyword)
            return resp['data']
        else:
            return []

    def search_space_ids(self, space_ids: str) -> list:
        """Retrieves data about spaces based on space ids.

        Args:
            space_ids: A list of space ids seperated by comma.

        Returns:
            A list of objects containing space information.
        """

        params = {
            'ids': space_ids,
            'expansions': 'invited_user_ids,speaker_ids,creator_id,host_ids',
            'space.fields': 'invited_user_ids,host_ids,created_at,creator_id,participant_count,speaker_ids,started_at,ended_at,title,scheduled_start,state',
            'user.fields': 'id,name,username,profile_image_url,public_metrics'
        }
        response = self.get(self.base_url, params)
        if 'data' in response.json().keys():
            resp = response.json()
            self.convert_user_id(resp)
            return resp['data']
        else:
            return []

    def search_creator_ids(self, hashtag: str, user_ids: str) -> list:
        """Retrieves data about spaces based on creator ids.

        Args:
            user_ids: A list of space ids seperated by comma.

        Returns:
            A list of objects containing space information.
        """

        url = f'{self.base_url}/by/creator_ids'
        params = {
            'user_ids': user_ids,
            'state': 'live',
            'expansions': 'invited_user_ids,speaker_ids,creator_id,host_ids',
            'space.fields': 'invited_user_ids,host_ids,created_at,creator_id,participant_count,speaker_ids,started_at,ended_at,title,scheduled_start,state',
            'user.fields': 'id,name,username,profile_image_url,public_metrics'
        }
        response = self.get(url, params)
        if 'data' in response.json().keys():
            resp = response.json()
            self.convert_user_id(resp)
            self.parse_keywords(resp, keyword=hashtag)
            return resp['data']
        else:
            return []

    def place_user_by_group(self, key: str, space: dict, mapped_users: dict) -> None:
        """Check if a Twitter space dict has a specific key and update the values.

        Args:
            key: Dictionary key.
            space: Twitter space data.
            mapped_users: Object containing Twitter users data.
        """
        if key in space.keys():
            space[key] = [mapped_users[host_id] for host_id in space[key]]


    def convert_user_id(self, resp: list) -> None:
        """Converts Twitter user ID into object with name, username, avatar, and ID.

        Args:
            resp: Response data from Twitter spaces API.
        """
        users = resp['includes']['users']
        mapped_users = {}
        for user in users:
            mapped_users[user['id']] = user

        try:
            for space in resp['data']:
                if 'creator_id' in space.keys():
                    space['creator_id'] = mapped_users[space['creator_id']]
                self.place_user_by_group('host_ids', space, mapped_users)
                self.place_user_by_group('invited_user_ids', space, mapped_users)
                self.place_user_by_group('speaker_ids', space, mapped_users)
        except(KeyError):
            # get the host id and look them up to then grab their information and create the creator_id values
            print('MISSING CREATOR - >>>>>>>>>>', space)


    def parse_keywords(self, resp: dict, keyword=None) -> None:
        """Parses hashtags from title of the space.

        Args:
            resp: Twitter space data.
        """
        for space in resp['data']:
            if 'title' in space.keys():
                tags = re.findall("#([a-zA-Z0-9_]+)", space['title'])
                upper_tags = list(map(lambda x: x.upper(), tags))
                upper_tag = keyword.upper()
                if keyword and upper_tag not in upper_tags:
                    upper_tags.append(upper_tag)
                space['tags'] = upper_tags
