import json
import requests


class Http:
    """A wrapper for python requests library.

    Attributes:
        key_path (str): A full path to a file.
    """
    def __init__(self, key_path: str) -> None:
        """Initializes a new Http with a path to a JSON file containing authentication key.
        """
        self.header = {'Authorization': f'Bearer {self.get_token(key_path)}'}

    def get_token(self, token: str) -> str:
        """Open json file and extract data.

        Args:
            key_path (str): A full path to a file.

        Returns:
            An object containing the token data.
        """
        return token

    def get(self, url: str, parameters: dict) -> object:
        """Make a GET HTTP request.

        Args:
            url: URL to call in the request.
            parameters: URL parameters.
            
        Returns:
            A GET request object.
        """
        return requests.get(url, headers=self.header, params=parameters)
