from .http import Http

class User(Http):
    """A wrapper for Twitter users API."""

    base_url = 'https://api.twitter.com/2'

    def get_by_id(self, user_ids: str) -> list:
        """Calls the users endpoint returning user information (id, name, username, profile_image_url, url)

        Args:
            user_ids: A list of user ids seperated by comma.

        Returns:
            A list of objects containing user information.
        """
        url = f'{self.base_url}/users'
        params = {
            'ids': user_ids,
            'user.fields': 'id,name,username,profile_image_url,url'
        }
        response = self.get(url, params)
        if 'data' in response.json().keys():
            return response.json()['data']
        else:
            return []
