import os
import logging
from typing import Optional, Dict, Any, List
from blockchain.blockfrost import BlockfrostAPI
from blockchain.gomaestro import MaestroAPI
from utils.general import filter_keys


class User:
    """
    A class representing a user of the blockchain application.

    Attributes:
        staking_address (str): The user's staking address.
        gomaestro_api (MaestroAPI): The Maestro API client used for interacting with the blockchain.
        blockfrost_api (BlockfrostAPI): The Blockfrost API client used for interacting with the blockchain.
    """

    def __init__(self, staking_address: str) -> None:
        """
        Initializes a new instance of the User class.

        Args:
            staking_address (str): The user's staking address.
        """
        self.staking_address = staking_address
        self.gomaestro_api = self._get_gomaestro_api()
        self.blockfrost_api = self._get_blockfrost_api()

    def _get_gomaestro_api(self) -> MaestroAPI:
        """
        Gets an instance of the MaestroAPI client.

        Returns:
            MaestroAPI: An instance of the MaestroAPI client.
        Raises:
            ValueError: If the Gomaestro API key is not found in environment variables.
        """
        gomaestro_api_key = os.environ.get('GOMAESTRO_API')
        if not gomaestro_api_key:
            raise ValueError(
                'Gomaestro API key not found in environment variables')
        return MaestroAPI(gomaestro_api_key, self.staking_address)

    def _get_blockfrost_api(self) -> BlockfrostAPI:
        """
        Gets an instance of the BlockfrostAPI client.

        Returns:
            BlockfrostAPI: An instance of the BlockfrostAPI client.
        Raises:
            ValueError: If the Blockfrost API key is not found in environment variables.
        """
        blockfrost_api_key = os.environ.get('BLOCKFROST_API')
        if not blockfrost_api_key:
            raise ValueError(
                'Blockfrost API key not found in environment variables')
        return BlockfrostAPI(blockfrost_api_key, self.staking_address)

    def get_account_information(self) -> Dict[str, Any]:
        """
        Gets the account information of the user from the Maestro API.

        Returns:
            dict or None: A dictionary containing the user's account information, or None if an error occurred.
        """
        try:
            account_data = self.gomaestro_api.get_account()
            return account_data
        except ValueError as e:
            logging.error(f'Error getting account information: {e}')
        except Exception as e:
            logging.error(f'Unknown error getting account information: {e}')
        return {}

    def get_tokens(self, count: int, page: int) -> List[Dict[str, Any]]:
        """
        Gets the user's tokens.

        Args:
            count (int): The number of assets to retrieve.
            page (int): The page number of assets to retrieve.

        Returns:
            List[Dict[str, Any]]: A list of dictionaries containing the user's assets.
        """
        try:
            # raw data of wallet assets
            assets = self.gomaestro_api.get_assets(count=count, page=page)
            # for each asset fetch metadata based on the policy id
            raw_assets = []
            for asset in assets:
                asset_metadata = self.blockfrost_api.get_asset(
                    asset['unit'].replace('#', ''))
                if asset_metadata[0]['metadata'] and asset_metadata[0]['metadata']['decimals']:
                    asset_metadata[0]['holdings'] = asset['quantity']
                    asset_metadata[0]['metadata']['logo'] = 'xxx'
                    raw_assets.append(asset_metadata[0])
            # for each asset filter only the keywords that are important
            filtered_assets = filter_keys(['asset', 'policy_id', 'asset_name', 'quantity',
                                           'metadata', 'holdings'], raw_assets)
            return [{'tokens': filtered_assets, 'count': len(filtered_assets)}]
        except ValueError as e:
            logging.error(f'Error getting assets: {e}')
        except Exception as e:
            logging.error(f'Unknown error getting assets: {e}')
        return []

    def get_nfts(self, count: int, page: int) -> List[Dict[str, Any]]:
        """
        Gets the user's nfts.

        Args:
            count (int): The number of assets to retrieve.
            page (int): The page number of assets to retrieve.

        Returns:
            List[Dict[str, Any]]: A list of dictionaries containing the user's assets.
        """
        try:
            # raw data of wallet assets
            assets = self.gomaestro_api.get_assets(count=count, page=page)
            # for each asset fetch metadata based on the policy id
            raw_assets = []
            for asset in assets:
                asset_metadata = self.blockfrost_api.get_asset(
                    asset['unit'].replace('#', ''))
                if asset_metadata[0]['metadata'] is None:
                    asset_metadata[0]['holdings'] = asset['quantity']
                    raw_assets.append(asset_metadata[0])
            # for each asset filter only the keywords that are important
            filtered_assets = filter_keys(['asset', 'policy_id', 'asset_name', 'quantity',
                                           'onchain_metadata', 'holdings'], raw_assets)
            return [{'tokens': filtered_assets, 'count': len(filtered_assets)}]
        except ValueError as e:
            logging.error(f'Error getting assets: {e}')
        except Exception as e:
            logging.error(f'Unknown error getting assets: {e}')
        return []

    def get_rewards(self, count: int, page: int) -> List[Dict[str, Any]]:
        """
        Returns a list of dictionaries containing the reward history for a given staking address.

        Args:
            count (int): The number of reward entries to return.
            page (int): The page of rewards to retrieve.

        Returns:
            list: A list of dictionaries containing the reward history for the given staking address.
                  Each dictionary includes the pool ID, pool name, ticker, amount, and transaction date.
                  If an error occurs, an empty list is returned.
        """
        try:
            # raw data of reward history
            rewards = self.gomaestro_api.get_staking_rewards(count, page)
            # unique set of pool ids
            pool_ids = {reward['pool_id'] for reward in rewards}
            # assign pool id to name and ticker
            clean_pool_ids = {}
            for pool_id in pool_ids:
                pool_metadata = self.gomaestro_api.get_pool_metadata(pool_id)
                if pool_metadata['meta_json']:
                    clean_pool_ids[pool_id] = {
                        'name': pool_metadata['meta_json']['name'], 'ticker': pool_metadata['meta_json']['ticker']}
                else:
                    clean_pool_ids[pool_id] = {'name': '', 'ticker': ''}
            # save ticker and name to pool data
            clean_rewards_data = [
                {**reward, **clean_pool_ids[reward['pool_id']]} for reward in rewards]
            return clean_rewards_data
        except ValueError as e:
            logging.error(f'Error getting assets: {e}')
        except Exception as e:
            logging.error(f'Unknown error getting assets: {e}')
        return []

    def get_current_delegation(self, pool_id: str) -> Dict[str, Any]:
        """
        Returns the current delegation for a given pool.

        Args:
            pool_id (str): The pool ID to retrieve delegation information for.

        Returns:
            dict: A dictionary containing the delegation information for the given pool,
                  including the stake address, delegated amount, and last update time.
        """
        pool_metadata = self.gomaestro_api.get_pool_information(pool_id)
        return pool_metadata


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    stake_address = 'stake1u8utkdjgzrhhfghdtz4sgp0j5exy54z2h074eudd4u5c4mskp2ewq'
    user = User(stake_address)
    account_data = user.get_account_information()
    account_assets = user.get_tokens(count=100, page=1)
    account_nfts = user.get_nfts(count=100, page=1)
    account_rewards = user.get_rewards(count=100, page=1)
    account_delegation = user.get_current_delegation(
        account_data['delegated_pool'])

    if account_data:
        logging.info(f'Account Information: {account_data}')
    if account_assets:
        logging.info(f'Account Tokens: {account_assets}')
    if account_nfts:
        logging.info(f'Account NFTS: {account_nfts}')
    if account_rewards:
        logging.info(f'Account Rewards: {account_rewards}')
    if account_delegation:
        logging.info(f'Account Delegation: {account_delegation}')
