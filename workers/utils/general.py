from typing import List, Dict, Any


def filter_keys(keys_to_keep: List[str], list_of_data: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
    """
    Filters a list of dictionaries by keeping only the specified keys for each dictionary.

    Args:
        keys_to_keep (List[str]): A list of keys to keep in the filtered dictionaries.
        list_of_data (List[Dict[str, Any]]): A list of dictionaries containing the objects to be filtered.

    Returns:
        List[Dict[str, Any]]: A list of dictionaries that contain only the specified keys and values.

    Example:
        original_list = [{'a': 1, 'b': 2, 'c': 3}, {'a': 4, 'b': 5, 'c': 6}]
        keys_to_keep = ['a', 'c']
        filtered_list = filter_keys(keys_to_keep, original_list)
        # filtered_list is now [{'a': 1, 'c': 3}, {'a': 4, 'c': 6}]
    """
    filtered_assets = []
    for obj in list_of_data:
        new_dict = {key: obj[key] for key in keys_to_keep}
        filtered_assets.append(new_dict)
    return filtered_assets
